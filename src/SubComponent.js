import React, { Component } from 'react';
import intl from 'react-intl-universal';

class SubComponent extends Component {
    render() {
        return (
            <div>
                <p>{intl.get('lorem')}</p>
            </div>
        );
    }
}

export default SubComponent;
