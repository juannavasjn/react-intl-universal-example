import React, { Component } from 'react';
import logo from './logo.svg';
import intl from 'react-intl-universal';
import SubComponent from './SubComponent';
import './App.css';
// app locale data

const locales = {
    en: require('./locales/en.json'),
    es: require('./locales/es.json'),
    ge: require('./locales/ge.json'),
    pt: require('./locales/pt.json')
};

class App extends Component {
    state = { initDone: false };

    componentDidMount() {
        this.loadLocales('en');
    }
    loadLocales(lan) {
        // init method will load CLDR locale data according to currentLocale
        // react-intl-universal is singleton, so you should init it only once in your app
        intl.init({
            currentLocale: lan, // TODO: determine locale here
            locales
        }).then(() => {
            // After loading CLDR locale data, start to render
            this.setState({ initDone: true });
        });
    }
    handleChange = lan => {
        this.loadLocales(lan);
    };
    render() {
        return (
            this.state.initDone && (
                <div className="App">
                    <header className="App-header">
                        <h3>{intl.get('lang')}</h3>
                        <img
                            src={require('./img/ve.jpeg')}
                            alt="es"
                            width="40"
                            onClick={_ => this.handleChange('es')}
                        />
                        <img
                            src={require('./img/ge.jpeg')}
                            alt="es"
                            width="40"
                            onClick={_ => this.handleChange('ge')}
                        />
                        <img
                            src={require('./img/en.png')}
                            alt="es"
                            width="40"
                            onClick={_ => this.handleChange('en')}
                        />
                        <img
                            src={require('./img/pt.png')}
                            alt="es"
                            width="40"
                            onClick={_ => this.handleChange('pt')}
                        />
                        <img src={logo} className="App-logo" alt="logo" />
                        <p>{intl.get('text')}</p>
                        <SubComponent />
                    </header>
                </div>
            )
        );
    }
}

export default App;
